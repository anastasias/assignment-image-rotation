#include "../include/file.h"
#include "../include/bmp.h"
#include "../include/rotate.h"
#include <stdlib.h>

int main(int argc, char **argv) {

    if ( argc != 3 ){
        fprintf(stderr, "not enough args");
        return -1;
    }
    
    char* const inputf = argv[1];
    char* const outputf= argv[2];


    FILE *input = NULL;
    FILE *output= NULL;


    struct image *img = image_create(0, 0);
    
    if(!open_file(&input, inputf, "r") ){
        printf("Can't open file for reading");
        return -1;
    }


    enum read_status status = from_bmp(input, img);

    if (status !=READ_OK){
        printf("Can't read the input file");
        image_destroy(img);
        if (close_file(&input)){
            printf("Can't close the input file");
            return -1;
        }
        return -1;
    }

    if(!(open_file(&output, outputf, "w") )){
        image_destroy(img);
        if (close_file(&input)){
            printf("Can't close the input file");
            return -1;
        }
        printf( "Can't open file for writing");
        return -1;
    }


    struct image* rotated = rotate(img);

    enum write_status status1 = to_bmp(output, rotated);

    if (status1!= WRITE_OK){
        printf("Can't write to the output file");
        image_destroy(img);
        image_destroy(rotated);
        return 0;
    }

    if(!close_file(&input)){
        printf("Can't close the input file");
        image_destroy(img);
        image_destroy(rotated);
        return -1;
    };

    if(!close_file(&output)){
        printf("Can't close the output file");
        image_destroy(img);
        image_destroy(rotated);
        return -1;
    }

    image_destroy(img);
    image_destroy(rotated);




    return 0;
}
