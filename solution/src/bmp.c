#include "../include/bmp.h"
static const uint16_t BMP_FILE_SIGNATURE = 0x4d42;
static const uint32_t HEADER_INFO_SIZE = 40;
static const uint16_t BITS_PER_PIXEL = 24;

static inline uint32_t get_padding(uint32_t width) { return width % 4; }
static struct header generate_header(uint32_t width, uint32_t height) {
  const uint32_t head_size = sizeof(struct header);
  const uint32_t img_size = sizeof(struct pixel)
            * height * (width + get_padding(width));
  const uint32_t file_size = head_size + img_size;
    return (struct header) {
            .bfType = BMP_FILE_SIGNATURE,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct header),
            .biSize = HEADER_INFO_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = 0,
            .biSizeImage = img_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
  };
}

enum read_status read_header(FILE *in, struct header *header) {

    enum read_status val = READ_OK;
    if (fread(header, sizeof(struct header), 1, in) < 1 ||
        header->biSize != HEADER_INFO_SIZE || header->biPlanes != 1 ||
        header->biBitCount != BITS_PER_PIXEL || header->biCompression != 0)
        val = READ_INVALID_HEADER;
    else if (header->bfType != BMP_FILE_SIGNATURE)
        val = READ_INVALID_SIGNATURE;
    return val;
}

enum read_status read_image(FILE *in, struct image *img) {
  for (uint32_t i = 0; i < img->height; i++) {
    if (fread(&img->pixels[i * img->width], sizeof(struct pixel), img->width, in)
            < img->width || fseek(in, get_padding(img->width), SEEK_CUR) != 0) {
      return READ_INVALID_BITS;
    }
  }
  return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
  struct header header = {0};
  enum read_status status = read_header(in, &header);
  if(status != 0){
    return status;
  }
  fseek(in,header.bOffBits,SEEK_SET);
  img->width = header.biWidth;
  img->height = header.biHeight;
  img->pixels = malloc(sizeof(struct pixel)*img->height*img->width);
  status = read_image(in, img);
  if( status != 0){
    return status;
  }
  return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image  *img) {
  const struct header header = generate_header(img->width, img->height);
  fwrite(&header, sizeof(struct header), 1, out);
  for (uint32_t o = 0, i = 0; i < img->height; i++) {
    if (fwrite(&img->pixels[i * img->width], sizeof(struct pixel), img->width, out)
            < header.biWidth || fwrite(&o, get_padding(img->width), 1, out) < 1) {
      return WRITE_ERROR;
    }
  }
  return WRITE_OK;
}


