#include "file.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool open_file (  FILE** file,const char* name, const char* mode ){

    *file = fopen(name, mode);
    if (*file == NULL) return false;
    return true;

}


bool close_file (FILE** file) {
    if (fclose (*file) == EOF){
        return false;
    }

    return true;
}



